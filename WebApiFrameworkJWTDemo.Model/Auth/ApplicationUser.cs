﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace WebApiFrameworkJWTDemo.Model
{
    public class ApplicationUser : IPrincipal
    {
        public ApplicationUser(string id, string name, DateTime expiration)
        {
            Identity = new UserIdentity(id, name, expiration);
        }
        public IIdentity Identity { get; }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}
