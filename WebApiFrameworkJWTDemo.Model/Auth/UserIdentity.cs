﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace WebApiFrameworkJWTDemo.Model
{
    public class UserIdentity : IIdentity
    {
        public UserIdentity(string id, string name, DateTime expiration)
        {
            Id = id;
            Name = name;
            Expiration = expiration;
        }
        public string Id { get; }
        public string Name { get; }
        public DateTime Expiration { get; set; }
        public string AuthenticationType { get; }

        public bool IsAuthenticated { get; }
    }
}
