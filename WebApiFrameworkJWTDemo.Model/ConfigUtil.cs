﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiFrameworkJWTDemo.Model
{
    public class ConfigUtil
    {
        /// <summary>
        /// JWT的Token有效期
        /// </summary>
        public static int TokenValidMinutes = 2;
        /// <summary>
        /// JWT的Token失效缓冲期
        /// </summary>
        public static int TokenCacheMinutes = 5;
    }
}
