﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiFrameworkJWTDemo.Model
{
    public class LoginViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "用户名不能为空")]
        public string LoginName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "密码不能为空")]
        public string Password { get; set; }
        public DateTime Expiration { get; set; } = DateTime.Now.AddMinutes(ConfigUtil.TokenValidMinutes);
        public string Email { get; set; }
    }
}
