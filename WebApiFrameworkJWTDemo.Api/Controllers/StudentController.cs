﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiFrameworkJWTDemo.Model;

namespace WebApiFrameworkJWTDemo.Api.Controllers
{
    public class StudentController : ApiController
    {
        public Student Get()
        {
            //return "Get()";
            return new Student() { Id = 1, Name = "ZhangSan" };
        }

        public string Get(string name)
        {
            return "name=" + name;
        }

        public string GetAddress(string address)
        {
            return "address=" + address;
        }

        public string Post(Student student, int basic1, string basic2)
        {
            return "Post(), student.id=" + student.Id + ", student.name=" + student.Name + ", basic1=" + basic1 + ", basic2=" + basic2;
        }
    }
}
